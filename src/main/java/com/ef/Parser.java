package com.ef;

import java.util.Collection;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.ef.parser.FileParser;
import com.ef.parser.RequestStrategy;
import com.ef.parser.dao.WebServerAccessLogDao;
import com.ef.parser.dto.FileParamsDto;
import com.ef.parser.entity.WebServerAccessLog;
import com.ef.parser.strategy.IpRequestStrategy;

public class Parser {

	private static final Logger LOG = LogManager.getLogger(Parser.class);
	
	private FileParser fileParser;	
	private RequestStrategy requestStrategy;
	
	
	public Parser(FileParser fileParser, RequestStrategy strategy) {
		this.fileParser = fileParser;
		this.requestStrategy = strategy;
	}
	
	public static void main(String[] args) {
		String logFilePath = getParamValue(args[0]);
		String startDate = getParamValue(args[1]);
		String duration = getParamValue(args[2]);
		String threshold = getParamValue(args[3]);
		
		WebServerAccessLogDao webServerAccessLogDao = new WebServerAccessLogDao();
		FileParser fileParser = new FileParser(webServerAccessLogDao);
		RequestStrategy strategy = new IpRequestStrategy(webServerAccessLogDao);
		new Parser(fileParser, strategy).process(startDate, duration, threshold, logFilePath);
	}

	private static String getParamValue(String param) {
		return param.split("=")[1];
	}

	private void process(String startDate, String duration, String threshold, String logFilePath) {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		FileParamsDto fileParams = new FileParamsDto(startDate, duration, threshold);
		for (ConstraintViolation<FileParamsDto> violation : validator.validate(fileParams)) {
		    LOG.warn(violation.getMessage()); 
		}		
		Collection<WebServerAccessLog> webServerLogs = fileParser.parseLogFile(logFilePath);
		requestStrategy.extractLogInfo(fileParams, webServerLogs);
	}

}
