package com.ef.parser;

import java.util.Collection;

import com.ef.parser.dto.FileParamsDto;
import com.ef.parser.entity.WebServerAccessLog;

public interface RequestStrategy {

	void extractLogInfo(FileParamsDto params, Collection<WebServerAccessLog> logs);
}
