package com.ef.parser.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.ef.parser.entity.BlockedIP;
import com.ef.parser.entity.WebServerAccessLog;
import com.ef.parser.util.HibernateUtil;

public class WebServerAccessLogDao {

	private Transaction transaction;

	public void insertBlockedIp(Session session, BlockedIP blockedIP) {
		session.save(blockedIP);
	}

	public void insertWebServerAccessLog(Session session, WebServerAccessLog webServerAccessLog) {
		session.save(webServerAccessLog);
	}

	public Session getSession() {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		return session;
	}

	public void startTransaction(Session session) {
		transaction = session.beginTransaction();
	}

	public void commit() {
		if (transaction == null) {
			throw new IllegalStateException("Transaction must be started [#startTransaction()]");
		}
		transaction.commit();
	}
	
	public void rollback() {
		if (transaction == null) {
			throw new IllegalStateException("Transaction must be started [#startTransaction()]");
		}
		transaction.rollback();
	}

	public void closeSession(SessionFactory sessionFactory) {
		sessionFactory.close();
	}

}
