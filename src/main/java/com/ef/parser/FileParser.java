package com.ef.parser;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;

import com.ef.parser.dao.WebServerAccessLogDao;
import com.ef.parser.entity.WebServerAccessLog;


public class FileParser {
	
	private static final Logger LOG = LogManager.getLogger(FileParser.class);
	
	private static final String LOG_FILE_DATE_PATTERN = "yyyy-MM-dd HH:mm:ss.SSS";

	private static final int DATE_INDEX = 0;
	private static final int IP_INDEX = 1;
	private static final int REQUEST_INDEX = 2;
	private static final int STATUS_INDEX = 3;
	private static final int USER_AGENT_INDEX = 4;

	private WebServerAccessLogDao webServerAccessLogDao;
	
	public FileParser(WebServerAccessLogDao webServerAccessLogDao) {
		this.webServerAccessLogDao = webServerAccessLogDao;
	}

	public Collection<WebServerAccessLog> parseLogFile(String logFilePath) {
		List<WebServerAccessLog> accessLogList = new ArrayList<>();
		try {
			Path path = Paths.get(logFilePath);
			List<String> requestLines = Files.readAllLines(path);
			Session session = webServerAccessLogDao.getSession();
			webServerAccessLogDao.startTransaction(session);
			for (String fileLine : requestLines) {
				String[] lineFields = fileLine.split("\\|");
				LocalDateTime date = LocalDateTime.parse(lineFields[DATE_INDEX], DateTimeFormatter.ofPattern(LOG_FILE_DATE_PATTERN));
				String ip = lineFields[IP_INDEX];
				String request = lineFields[REQUEST_INDEX];
				String status = lineFields[STATUS_INDEX];
				String userAgent = lineFields[USER_AGENT_INDEX];
				WebServerAccessLog webServerAccessLog = new WebServerAccessLog(date, ip, request, status, userAgent);
				webServerAccessLogDao.insertWebServerAccessLog(session, webServerAccessLog);
				accessLogList.add(webServerAccessLog);
			}
			webServerAccessLogDao.commit();
			return accessLogList;
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			accessLogList.clear();
			webServerAccessLogDao.rollback();
		}
		return Collections.emptyList();
	}

}
