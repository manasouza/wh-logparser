package com.ef.parser.strategy;

import static java.util.stream.Collectors.groupingBy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;

import com.ef.parser.RequestStrategy;
import com.ef.parser.dao.WebServerAccessLogDao;
import com.ef.parser.dto.FileParamsDto;
import com.ef.parser.entity.BlockedIP;
import com.ef.parser.entity.Duration;
import com.ef.parser.entity.WebServerAccessLog;


public class IpRequestStrategy implements RequestStrategy {

	private static final Logger LOG = LogManager.getLogger(IpRequestStrategy.class);
	
	private static final String DATE_FORMAT = "yyyy-MM-dd.HH:mm:ss";
	
	private WebServerAccessLogDao webServerAccessLogDao;

	public IpRequestStrategy(WebServerAccessLogDao webServerAccessLogDao) {
		this.webServerAccessLogDao = webServerAccessLogDao;
	}

	@Override
	public void extractLogInfo(FileParamsDto params, Collection<WebServerAccessLog> webServerLogs) {
		final LocalDateTime startDate = LocalDateTime.parse(params.getStartDate(), DateTimeFormatter.ofPattern(DATE_FORMAT));
		Duration duration = Duration.valueOf(params.getDuration().toUpperCase());
		Integer threshold = Integer.valueOf(params.getThreshold());
		Predicate<WebServerAccessLog> durationFilter = w -> w.getDate().isAfter(startDate) && w.getDate().isBefore(startDate.plus(getPeriod(duration))); 
		Map<String, List<WebServerAccessLog>> requestIpMap = webServerLogs.stream()
				.filter(durationFilter)
				.collect(groupingBy(WebServerAccessLog::getIp));
		try {
			Session session = webServerAccessLogDao.getSession();
			webServerAccessLogDao.startTransaction(session);
			for (String ip : requestIpMap.keySet()) {
				List<WebServerAccessLog> webServerLogList = requestIpMap.get(ip);
				if (webServerLogList.size() >= threshold) {
					BlockedIP blockedIP = new BlockedIP();
					blockedIP.setIp(ip);
					blockedIP.setComments(String.format("Blocked due to exceed %s requests", threshold));
					webServerAccessLogDao.insertBlockedIp(session, blockedIP);
					LOG.info(ip);
				}
			}
			webServerAccessLogDao.commit();
		} catch (Throwable t) {
			LOG.error(t.getMessage(), t);
			webServerAccessLogDao.rollback();
		}
	}

	private java.time.Duration getPeriod(Duration duration) {
		switch (duration) {
		case DAILY:
			return java.time.Duration.ofDays(1);
		case HOURLY:
			return java.time.Duration.ofHours(1);
		default:
			throw new IllegalArgumentException("Invalid duration value");
		}
	}

}
