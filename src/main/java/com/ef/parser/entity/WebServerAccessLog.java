package com.ef.parser.entity;

import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="webserver_access_log")
public class WebServerAccessLog {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(name="id")
	private int id;
	
	@Column(name="user_agent")
	private String userAgent;
	
	@Column(name="response_status")
	private String status;
	
	@Column(name="request")
	private String request;
	
	@Column(name="ip")
	private String ip;
	
	@Column(name="request_date")
	private LocalDateTime date;
	
	
	public WebServerAccessLog() {
	}

	public WebServerAccessLog(LocalDateTime date, String ip, String request, String status, String userAgent) {
		this.date = date;
		this.ip = ip;
		this.request = request;
		this.status = status;
		this.userAgent = userAgent;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public LocalDateTime getDate() {
		return date;
	}
	
	public void setDate(LocalDateTime date) {
		this.date = date;
	}
	
	public String getIp() {
		return ip;
	}
	
	public void setIp(String ip) {
		this.ip = ip;
	}
	
	public String getRequest() {
		return request;
	}
	
	
	public void setRequest(String request) {
		this.request = request;
	}
	
	
	public String getStatus() {
		return status;
	}
	
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	public String getUserAgent() {
		return userAgent;
	}
	
	
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == this) return true;
        if (!(obj instanceof WebServerAccessLog)) {
            return false;
        }
        WebServerAccessLog ws = (WebServerAccessLog) obj;
		return Objects.equals(this.date, ws.getDate())
				&& Objects.equals(this.ip, ws.getIp())
				&& Objects.equals(this.request, ws.getRequest())
				&& Objects.equals(this.status, ws.getStatus())
				&& Objects.equals(this.userAgent, ws.getUserAgent());
	}

	@Override
	public int hashCode() {
		return Objects.hash(date, ip, status, request, userAgent);
	}
}
