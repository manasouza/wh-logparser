package com.ef.parser.entity;

public enum Duration {
	HOURLY, DAILY
}