package com.ef.parser.dto;

import javax.validation.constraints.NotNull;

public class FileParamsDto {

	@NotNull
	private String startDate;
	@NotNull
	private String duration;
	@NotNull
	private String threshold;

	public FileParamsDto(String startDate, String duration, String threshold) {
		this.startDate = startDate;
		this.duration = duration;
		this.threshold = threshold;
	}
	
	public String getDuration() {
		return duration;
	}
	
	public String getStartDate() {
		return startDate;
	}
	
	public String getThreshold() {
		return threshold;
	}
}
